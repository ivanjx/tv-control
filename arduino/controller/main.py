import serial


def main():
  # Prompting bluetooth port.
  com_name = input('Bluetooth port name-> ')

  # Connecting.
  print('Connecting...')
  com = serial.Serial(com_name)
  print('Connected.')

  # Sending test command.
  print('Sending HELO...')
  com.write(b'HELO\n')

  # Reading response.
  print('Reading response...')
  resp = com.readline()
  print('Got response: ' + str(resp))

  # Test blink.
  print('Testing blink...')
  com.write(b'BLINK\n')
  com.write(b'10\n')
  com.write(b'1000\n')

  # Reading response.
  print('Reading response...')
  resp = com.readline()
  print('Got response: ' + str(resp))

  # Done.
  print("Done")


if __name__ == "__main__":
  main()