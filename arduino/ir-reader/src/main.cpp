#include <Arduino.h>
#include <IRremote.h>

const int SERIAL_BAUD_RATE = 9600;
const int IR_READER_PIN = 10;

IRrecv _irReader(IR_READER_PIN);

void setup() {
  // Initializing serial logger.
  Serial.begin(SERIAL_BAUD_RATE);

  // Activating ir reader.
  Serial.println("Activating IR reader...");
  _irReader.enableIRIn();
  Serial.println("IR reader activated.");
}

void dump(decode_results *results) {
  // Start declaration
  Serial.print("unsigned int  ");          // variable type
  Serial.print("rawData[");                // array name
  Serial.print(results->rawlen - 1, DEC);  // array size
  Serial.print("] = {");                   // Start declaration

  // Dump data
  for (int i = 1;  i < results->rawlen;  i++) {
    Serial.print(results->rawbuf[i] * USECPERTICK, DEC);
    if ( i < results->rawlen-1 ) Serial.print(","); // ',' not needed on last one
    if (!(i & 1))  Serial.print(" ");
  }

  // End declaration
  Serial.print("};");  // 
}

void loop() {
  // Decoding ir result.
  decode_results ir_result;

  if(_irReader.decode(&ir_result)) {
    // Printing result.
    dump(&ir_result);
    Serial.println();
    Serial.println();

    // Reading next value if any.
    _irReader.resume();
  }
}