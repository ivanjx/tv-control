#include <Arduino.h>
#include <IRremote.h>
#include <SoftwareSerial.h>

const int COMMAND_BUFFER_LENGTH = 1024;
const int SERIAL_LOGGER_BAUD = 9600;
const int IR_SENDER_HZ = 38;
const int BT_BAUD = 9600;
const int BT_RX_PIN = 12; // Connect to target's TX.
const int BT_TX_PIN = 13; // Connect to target's RX.
const int BLINK_PIN = 9;

IRsend _irSender;
SoftwareSerial _bt(BT_RX_PIN, BT_TX_PIN);
char _commandBuffer[COMMAND_BUFFER_LENGTH];

void setup() {
  // Enabling serial logging.
  Serial.begin(SERIAL_LOGGER_BAUD);
  Serial.println("Logger initialized.");

  // Enabling bluetooth port.
  _bt.begin(BT_BAUD);
  Serial.println("Bluetooth initialized.");

  // Enabling blinking pin
  pinMode(BLINK_PIN, OUTPUT);
}

int readSerialLine(SoftwareSerial *serial, char buff[], int buffLen) {
  int totalRead = 0;

  while(true) {
    if(!serial->available()) {
      // Read when ready.
      continue;
    }

    // Reading a char.
    char read = serial->read();
    
    // Checking.
    if(read == 0) {
      // Ignore NULL.
      continue;
    }

    if(read == '\n') {
      // Done.
      break;
    }

    buff[totalRead] = read;
    ++totalRead;

    if(buffLen == totalRead) {
      // Enough is enough.
      break;
    }
  }

  return totalRead;
}

void clearBuffer(char buff[], int buffLen) {
  for(int i = 1; i < buffLen; ++i) {
    buff[i] = 0;
  }
}

void handleUnknown(char cmd[]) {
  // Unknown.
  Serial.print("Unknown command: ");
  Serial.println(cmd);

  // Sending response.
  _bt.println("ERROR");
  _bt.println("Unknown command");
}

void handleHELO() {
  Serial.println("HELO received.");

  // Sending response.
  _bt.println("HELO");
}

void handleBLINK() {
  // Reading interval.
  clearBuffer(_commandBuffer, COMMAND_BUFFER_LENGTH);
  readSerialLine(&_bt, _commandBuffer, COMMAND_BUFFER_LENGTH);
  long interval = atol(_commandBuffer);

  // Reading delay per interval.
  clearBuffer(_commandBuffer, COMMAND_BUFFER_LENGTH);
  readSerialLine(&_bt, _commandBuffer, COMMAND_BUFFER_LENGTH);
  long intervalDelay = atol(_commandBuffer);

  Serial.print("Will blink ");
  Serial.print(interval);
  Serial.print(" times with ");
  Serial.print(intervalDelay);
  Serial.println(" ms delay.");

  // Sending response.
  _bt.println("OK");

  // Blinking.
  for(int i = 0; i < interval; ++i) {
    digitalWrite(BLINK_PIN, HIGH);
    delay(100);
    digitalWrite(BLINK_PIN, LOW);
    delay(intervalDelay);
  }

  // Done.
  Serial.println("Done blinking.");
}

void loop() {
  // Clearing buffer first.
  clearBuffer(_commandBuffer, COMMAND_BUFFER_LENGTH);
  
  // Reading response.
  int read = readSerialLine(&_bt, _commandBuffer, COMMAND_BUFFER_LENGTH);

  if(read == 0) {
    Serial.println("Read is 0");
    return;
  }

  Serial.print("Received: ");
  Serial.println(_commandBuffer);

  // Parsing command.
  if(strcmp(_commandBuffer, "HELO") == 0) {
    handleHELO();
  } else if(strcmp(_commandBuffer, "BLINK") == 0) {
    handleBLINK();
  } else {
    handleUnknown(_commandBuffer);
  }
}

/*
const unsigned int PREV_CH_BUFF[31] = {250,1800, 300,800, 250,800, 250,800, 300,750, 300,800, 250,1800, 250,800, 250,800, 300,1800, 250,800, 250,800, 300,800, 250,1800, 250,800, 300};
const unsigned int NEXT_CH_BUFF[31] = {300,1800, 250,800, 250,800, 300,800, 250,800, 250,800, 250,1800, 300,1750, 300,1750, 300,800, 250,1800, 250,1800, 300,1800, 250,800, 250,1800, 300};
const unsigned int VOL_UP_BUFF[31] = {250,1800, 300,800, 250,800, 250,800, 300,800, 250,800, 250,800, 250,1800, 300,800, 250,1800, 250,800, 300,800, 250,800, 250,1800, 300,800, 250};
const unsigned int VOL_DOWN_BUFF[31] = {300,1800, 250,800, 300,800, 250,800, 250,800, 300,1800, 250,800, 250,1800, 250,800, 250,1800, 300,800, 250,800, 250,800, 300,1800, 250,800, 250};
const unsigned int POWER_BUFF[31] = {300,1750, 300,800, 250,800, 300,750, 300,750, 300,800, 250,1800, 250,1800, 250,850, 250,1800, 250,800, 250,850, 250,800, 250,1800, 250,850, 250};
*/

/*
void sendIRCommand(const unsigned int buff[]) {
  for(int i = 0; i < 3; ++i) {
    _irSender.sendRaw(buff, 31, IR_SENDER_HZ);
    delay(100);
  }
}
*/